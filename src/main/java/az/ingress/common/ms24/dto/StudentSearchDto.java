package az.ingress.common.ms24.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class StudentSearchDto {

    private Long id;

    private String firstName;

    private String lastName;

    private String studentNumber;

    private Integer age;

    private String address;

    private String city;

    private String gender;

}
