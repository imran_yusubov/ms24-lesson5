package az.ingress.common.ms24.services;

import az.ingress.common.ms24.domain.StudentEntity;
import az.ingress.common.ms24.dto.StudentSearchDto;
import az.ingress.common.ms24.repository.StudentRepository;
import az.ingress.common.ms24.dto.StudentDto;
import az.ingress.common.ms24.dto.StudentRequestDto;
import az.ingress.common.ms24.exception.NotFoundException;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;

    @Override
    public StudentDto create(StudentRequestDto requestDto) {
        StudentEntity student =
                StudentEntity.
                        builder()
                        .firstName(requestDto.getFirstName())
                        .lastName(requestDto.getLastName())
                        .age(requestDto.getAge())
                        .studentNumber(requestDto.getStudentNumber())
                        .build();
        final StudentEntity studentEntitySaved = studentRepository.save(student);
        return StudentDto
                .builder()
                .id(studentEntitySaved.getId())
                .studentNumber(studentEntitySaved.getStudentNumber())
                .firstName(studentEntitySaved.getFirstName())
                .lastName(studentEntitySaved.getLastName())
                .age(studentEntitySaved.getAge())
                .build();
    }

    @Override
    public StudentDto update(Long id, StudentRequestDto requestDto) {
        StudentEntity student =
                StudentEntity.
                        builder()
                        .id(id)
                        .firstName(requestDto.getFirstName())
                        .lastName(requestDto.getLastName())
                        .age(requestDto.getAge())
                        .studentNumber(requestDto.getStudentNumber())
                        .build();
        final StudentEntity studentEntitySaved = studentRepository.save(student);
        return StudentDto
                .builder()
                .id(studentEntitySaved.getId())
                .studentNumber(studentEntitySaved.getStudentNumber())
                .firstName(studentEntitySaved.getFirstName())
                .lastName(studentEntitySaved.getLastName())
                .age(studentEntitySaved.getAge())
                .build();
    }

    @Override
    public StudentDto get(Long id) {
        final StudentEntity studentEntity = studentRepository.findById(id)
                .orElseThrow(NotFoundException::new);
        return StudentDto
                .builder()
                .id(studentEntity.getId())
                .studentNumber(studentEntity.getStudentNumber())
                .firstName(studentEntity.getFirstName())
                .lastName(studentEntity.getLastName())
                .age(studentEntity.getAge())
                .build();
    }

    @Override
    public void delete(Long id) {
        studentRepository.deleteById(id);
    }

    @Override
    public Page<StudentDto> list(StudentSearchDto searchDto, @PageableDefault(size = 5, page = 1) Pageable pageable) {

        studentRepository.findAll((Specification<StudentEntity>) (root, query, criteriaBuilder) -> {
            final Predicate[] specification = specification(searchDto, root, criteriaBuilder);
            return criteriaBuilder.and(specification);
        });


        return null;
    }

    private Predicate[] specification(StudentSearchDto dto, Root<StudentEntity> root, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicateList = new ArrayList<>();

        if (dto.getFirstName() != null) {
            final Predicate predicate = criteriaBuilder.equal(root.get(StudentEntity.Fields.firstName), dto.getFirstName());
            predicateList.add(predicate);
        }


        if (dto.getLastName() != null) {
            final Predicate predicate = criteriaBuilder.equal(root.get(StudentEntity.Fields.lastName), dto.getLastName());
            predicateList.add(predicate);
        }


        if (dto.getAge() != null) {
            final Predicate predicate = criteriaBuilder.equal(root.get(StudentEntity.Fields.age), dto.getAge());
            predicateList.add(predicate);
        }

        return predicateList.toArray(new Predicate[0]);
    }


}
