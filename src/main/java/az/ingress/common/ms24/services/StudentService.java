package az.ingress.common.ms24.services;

import az.ingress.common.ms24.dto.StudentDto;
import az.ingress.common.ms24.dto.StudentRequestDto;
import az.ingress.common.ms24.dto.StudentSearchDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface StudentService {


    StudentDto create(StudentRequestDto requestDto);

    StudentDto update(Long id, StudentRequestDto requestDto);

    StudentDto get(Long id);

    void delete(Long id);

    Page<StudentDto> list(StudentSearchDto searchDto, Pageable page);
}
