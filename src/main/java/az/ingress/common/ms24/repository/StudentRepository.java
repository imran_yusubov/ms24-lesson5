package az.ingress.common.ms24.repository;

import az.ingress.common.ms24.domain.StudentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface StudentRepository extends JpaRepository<StudentEntity, Long>, JpaSpecificationExecutor<StudentEntity> {

    //select * from students where id>id
    List<StudentEntity> findAllByIdGreaterThan(Long id);

    //List<StudentEntity> findAllByIdGreaterThan(Long id, String ad);

    //select * from students where id>:id and adi like :ad
    // findALl  --> select * from  students

    List<StudentEntity> findAllByFirstName(String firstName);

    List<StudentEntity> findAllByLastName(String lastName);

    List<StudentEntity> findAllByFirstNameAndLastName(String firstName, String lastName);

    List<StudentEntity> findAllByIdGreaterThanAndFirstNameLike(Long id, String firstName);

    List<StudentEntity> findAllByIdGreaterThanAndLastNameLike(Long id, String lastName);

    //JPQL
    @Query(value = "SELECT s FROM StudentEntity s WHERE s.firstName=?2 AND s.lastName=?1")
    List<StudentEntity> findByJpql(String firstName, String lastName);

    //SQL
    @Query(value = "SELECT s.first_name FROM students s", nativeQuery = true)
    List<String> findBySql();


}
