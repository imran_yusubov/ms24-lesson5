package az.ingress.common.ms24;

import az.ingress.common.ms24.domain.StudentEntity;
import az.ingress.common.ms24.dto.StudentDto;
import az.ingress.common.ms24.dto.StudentSearchDto;
import az.ingress.common.ms24.repository.StudentRepository;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@SpringBootApplication
public class Ms24Application implements CommandLineRunner {

    private final StudentRepository studentRepository;

    public static void main(String[] args) {
        SpringApplication.run(Ms24Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        StudentSearchDto searchDto =
                StudentSearchDto
                        .builder()
//                        .firstName("Atilla")
//                        .lastName("Memmedli")
//                        .age(20)
                        .build();
        final List<StudentEntity> repositoryAll = studentRepository.findAll((Specification<StudentEntity>) (root, query, criteriaBuilder) -> {
            final Predicate[] specification = specification(searchDto, root, criteriaBuilder);
            return criteriaBuilder.and(specification);
        });

        System.out.println(repositoryAll);

//        studentRepository.findAll(idGreaterThan(3L))
//                .stream()
//                .forEach(System.out::println);
        //     int id = 9;
        ///final Specification<StudentEntity> specification =;
//
//        studentRepository.findAll((root, query, criteriaBuilder) ->
//                criteriaBuilder.lessThan(root.get(StudentEntity.Fields.id), id));
    }

    public static Specification<StudentEntity> idGreaterThan(Long id) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.greaterThan(root.get(StudentEntity.Fields.id), id);
    }


    private StudentEntity atilla() {
        return StudentEntity
                .builder()
                .age(20)
                .firstName("Atilla")
                .lastName("Memmedli")
                .studentNumber("123456")
                .build();
    }

    private StudentEntity kamil() {
        return StudentEntity
                .builder()
                .age(20)
                .firstName("Kamil")
                .lastName("Ehmedov")
                .studentNumber("3456")
                .build();
    }

    private Predicate[] specification(StudentSearchDto dto, Root<StudentEntity> root, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicateList = new ArrayList<>();
        if (dto.getFirstName() != null) {
            final Predicate predicate = criteriaBuilder.equal(root.get(StudentEntity.Fields.firstName), dto.getFirstName());
            predicateList.add(predicate);
        }

        if (dto.getLastName() != null) {
            final Predicate predicate = criteriaBuilder.equal(root.get(StudentEntity.Fields.lastName), dto.getLastName());
            predicateList.add(predicate);
        }

        if (dto.getAge() != null) {
            final Predicate predicate = criteriaBuilder.equal(root.get(StudentEntity.Fields.age), dto.getAge());
            predicateList.add(predicate);
        }

        return predicateList.toArray(new Predicate[0]);
    }
}
