package az.ingress.common.ms24.rest;


import az.ingress.common.ms24.dto.StudentDto;
import az.ingress.common.ms24.dto.StudentRequestDto;
import az.ingress.common.ms24.dto.StudentSearchDto;
import az.ingress.common.ms24.services.StudentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/students")
public class StudentController {

    private final StudentService studentService;

    @GetMapping
    public Page<StudentDto> searchStudents(@RequestBody StudentSearchDto searchDto, Pageable page) {
        log.info("Received page {} size {}", page);
        return studentService.list(searchDto, page);
    }

    @GetMapping("/{id}")
    public StudentDto getStudent(@PathVariable Long id) {
        return studentService.get(id);
    }

    @PostMapping
    public StudentDto create(@RequestBody @Validated StudentRequestDto requestDto) {
        return studentService.create(requestDto);
    }

    @PutMapping("/{id}")
    public StudentDto update(
            @PathVariable Long id,
            @RequestBody StudentRequestDto requestDto) {
        return studentService.update(id, requestDto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteStudent(@PathVariable Long id) {
        studentService.delete(id);
    }

}
